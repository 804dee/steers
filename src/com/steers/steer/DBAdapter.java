package com.steers.steer;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBAdapter {

	public static final String KEY_USERID = "user_id";
	public static final String KEY_EMAIL = "email";
	public static final String KEY_PASSWORD = "password";

	public static final String KEY_ROWID = "_id";
	public static final String KEY_NAME = "name";
	public static final String KEY_PRICE = "price";
	public static final String KEY_QUANTITY = "quantity";
	public static final String KEY_DESC = "desc";

	private static final String DATABASE_NAME = "mduuka";
	private static final String DATABASE_TABLE = "cart";
	private static final String DATABASE_TABLE1 = "user";
	private static final int DATABASE_VERSION = 1;

	private static final String DATABASE_CREATE1 = "create table "
			+ DATABASE_TABLE1 + "("+KEY_USERID+" integer primary key, "
			+ KEY_EMAIL + " text not null, " + KEY_PASSWORD + " text not null);";
	
	private static final String DATABASE_CREATE = "create table "
			+ DATABASE_TABLE + " (_id integer primary key, "
			+ "name text not null, " + "price integer not null, "
			+ "quantity integer not null, " + "desc text not null);";

	private final Context context;
	private DatabaseHelper DBHelper;
	private SQLiteDatabase db;

	public DBAdapter(Context ctx) {
		this.context = ctx;
		DBHelper = new DatabaseHelper(context);
	}

	private static class DatabaseHelper extends SQLiteOpenHelper {

		public DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			try {
				db.execSQL(DATABASE_CREATE);
				db.execSQL(DATABASE_CREATE1);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
			db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE1);
			onCreate(db);
		}

	}

	// Open database connection
	public DBAdapter open() throws SQLException {
		db = DBHelper.getWritableDatabase();
		return this;
	}

	// database connection
	public void close() {
		db.close();
	}

	// USER
	// insert into users
	public void insertUser(int userId, String email, String password) {
		ContentValues input = new ContentValues();
		input.put(KEY_USERID, userId);
		input.put(KEY_EMAIL, email);
		input.put(KEY_PASSWORD, password);

		db.insert(DATABASE_TABLE1, null, input);
	}

	// check if user exists
	public boolean isUserLogged() {
		if (db.query(DATABASE_TABLE1, new String[] { KEY_USERID, KEY_EMAIL},
				null, null, null, null, null).moveToFirst()) {
			return true;
		} else {
			return false;
		}
	}

	// CART
	// insert item in cart
	public boolean insertItem(int id, String name, int price, String desc) {
		ContentValues insertValues = new ContentValues();
		insertValues.put(KEY_ROWID, id);
		insertValues.put(KEY_NAME, name);
		insertValues.put(KEY_QUANTITY, 1);
		insertValues.put(KEY_PRICE, price);
		insertValues.put(KEY_DESC, desc);

		return db.insert(DATABASE_TABLE, null, insertValues) > 0;
	}

	// check if item is on cart
	public boolean isItemInCart(int pid) {
		Cursor c = db.query(DATABASE_TABLE, new String[] { KEY_ROWID },
				KEY_ROWID + " = " + pid, null, null, null, null);
		if (c.moveToFirst()) {
			return true;
		} else {
			return false;
		}
	}

	// return all items
	public Cursor getAllItems() {
		return db.query(DATABASE_TABLE, new String[] { KEY_ROWID, KEY_NAME,
				KEY_PRICE, KEY_QUANTITY, KEY_DESC }, null, null, null, null,
				null);
	}

	// delete an item from the cart
	public boolean DeleteItem(int pid) {
		return db.delete(DATABASE_TABLE, KEY_ROWID + " = " + pid, null) > 0;
	}
}
