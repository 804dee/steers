package com.steers.steer;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Register extends Activity{
	EditText fullName, phone, email, password;
	Button register;
	ProgressDialog pDialog;
	DBAdapter insert = new DBAdapter(Register.this);
	DBAdapter check = new DBAdapter(Register.this);
	
	JSONP jp = new JSONP();
	private static String url_create_customer= "http://192.168.43.65:81/mduuka/api/create_customer.php";
	
	private static final String TAG_SUCCESS="success";
	//private static final String TAG_CUSTID ="custid";
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.register);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title);
		
		check.open();
		if(check.isUserLogged()){
			check.close();
			startActivity(new Intent(Register.this, Home.class));
			finish();
		}
		check.close();
		TextView Register = (TextView) findViewById(R.id.tvDetails);
		Register.setText("Register");
		
		fullName = (EditText) findViewById(R.id.etPersonsName);
		phone = (EditText) findViewById(R.id.etPhone);
		email = (EditText) findViewById(R.id.etRegisterEmail);
		password= (EditText) findViewById(R.id.etRegisterPassword);
		
		
		register = (Button) findViewById(R.id.btnRegister);
		register.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				new CreateNewCustomer().execute();
			}
		});
	}
	
	/*@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK){
			startActivity(new Intent(getApplicationContext(), Login.class));
			finish();
		}
		
		return false;
	}*/

	class CreateNewCustomer extends AsyncTask<String,String,String>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Register.this);
			pDialog.setMessage("Creating Account...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}
		
		@Override
		protected String doInBackground(String... arg0) {
			Encription encrpto = new Encription(password.getText().toString());
			
			String InputName = fullName.getText().toString();
			String InputPhone = phone.getText().toString();
			String Inputemail = email.getText().toString();
			String Inputpassword = encrpto.md5();
			
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("fullname",InputName));
			params.add(new BasicNameValuePair("phone",InputPhone));
			params.add(new BasicNameValuePair("email",Inputemail));
			params.add(new BasicNameValuePair("password",Inputpassword));
			
			
			JSONObject json = jp.makeHttpRequest(url_create_customer, "POST", params);
			
			Log.d("Create Response", json.toString());
			
			try{
				int success = json.getInt(TAG_SUCCESS);
				
				
				if (success != 0){
					insert.open();
					insert.insertUser(success, Inputemail, Inputpassword);
					insert.close();
					
					startActivity(new Intent(getApplicationContext(),Home.class));
					finish();
				}
			}catch(JSONException e){
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			pDialog.dismiss();
		}

		
	}

}
