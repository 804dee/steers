package com.steers.steer;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class Cart extends ListActivity {

	ArrayList<HashMap<String, String>> cartList;
	Cursor c;
	

	private static final String TAG_PID = "_id";
	private static final String TAG_NAME = "name";
	private static final String TAG_PRICE = "price";
	private static final String TAG_QTY = "qty";
	private static final String TAG_DESC = "desc";
	int total = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cart);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title);
		
		// Title Bar text 
		TextView List = (TextView) findViewById(R.id.tvDetails);
		List.setText("Send order");
		
		List.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
			
				
			}
		});
		
		// Title image view button
		ImageView home = (ImageView) findViewById(R.id.header);
		home.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getApplicationContext(), Home.class));
			}
		});
		
		cartList = new ArrayList<HashMap<String, String>>();
		
		// List View 
		ListView listview = getListView();
		listview.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				String name = ((TextView) view.findViewById(R.id.tvCartname)).getText().toString();
				Toast.makeText(getApplicationContext(), name, Toast.LENGTH_SHORT).show();
			}
			
		});
		
		// query database for items in the shopping cart
		DBAdapter db = new DBAdapter(getApplicationContext());
		db.open();
		c = db.getAllItems();

		if (c.moveToFirst()) {
			do {
				total += c.getInt(0);
				String pid = c.getString(0);
				String name = c.getString(1);
				String price = c.getString(2);
				String qty = c.getString(3);
				String desc = c.getString(4);

				HashMap<String, String> map = new HashMap<String, String>();

				map.put(TAG_PID, pid);
				map.put(TAG_NAME, name);
				map.put(TAG_PRICE, price);
				map.put(TAG_QTY, qty);
				map.put(TAG_DESC, desc);

				cartList.add(map);

			} while (c.moveToNext());
		}
		startManagingCursor(c);
		c.close();
		db.close();
		
		ListAdapter adapter = new SimpleAdapter(Cart.this, cartList,
				R.layout.cart_list, new String[] { TAG_PID, TAG_NAME,
						TAG_PRICE, TAG_QTY, TAG_DESC }, new int[] {
						R.id.tvCartpid, R.id.tvCartname, R.id.tvCartprice,
						R.id.tvCartQty, R.id.tvCartdesc });

		setListAdapter(adapter);

	}

	// back button
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK){
			startManagingCursor(c);
			c.close();
			startActivity(new Intent(getApplicationContext(),Home.class));
			finish();
		}
		return false;
	}
	
	

}
