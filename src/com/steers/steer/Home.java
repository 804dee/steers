package com.steers.steer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class Home extends ListActivity {
	private ProgressDialog pDialog;
	protected ImageView viewCart;
	protected TextView details,text;
	ConnectionDetector is_internet_connected;
	/*
	 * private String prefName = "LoginDetails"; private static final String
	 * EMAIL = "email"; private static final String PASSWORD = "password";
	 * SharedPreferences prefs;
	 */

	JSONP jp = new JSONP();
	private static final String TAG_PRODUCTS = "products";
	private static final String TAG_SUCCESS = "success";
	private static final String TAG_NAME = "prodname";
	private static final String TAG_PRICE = "price";
	private static final String TAG_PID = "prodid";
	private static final String TAG_DESC = "productdesc";

	private static String url_products = "http://192.168.43.65:81/mduuka/api/products.php";

	ArrayList<HashMap<String, String>> productList;
	//ArrayList<HashMap<String, String>> cart;
	JSONArray products = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title);
		
		
		// Title bar 
		TextView menu = (TextView) findViewById(R.id.tvDetails);
		menu.setText("Menu");

		// Title bar cart button
		viewCart = (ImageView) findViewById(R.id.refresh);
		viewCart.setImageResource(R.drawable.arrow_circle_down);
		viewCart.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Home.this, Cart.class));
				finish();
			}
		});

		productList = new ArrayList<HashMap<String, String>>();
		//cart = new ArrayList<HashMap<String, String>>();

		
			new LoadProducts().execute();
		
			
		
		// ListView for the products	
		ListView lv = getListView();

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int positon, long id) {

				int pid = Integer.parseInt(((TextView) view
						.findViewById(R.id.pid)).getText().toString());
				String name = ((TextView) view.findViewById(R.id.name))
						.getText().toString();
				int price = Integer.parseInt(((TextView) view
						.findViewById(R.id.price)).getText().toString());
				String desc = ((TextView) view.findViewById(R.id.desc))
						.getText().toString();
				 text = (TextView) view.findViewById(R.id.desc);
				DBAdapter insert = new DBAdapter(Home.this);
				insert.open();

				if (insert.isItemInCart(pid)) {
					insert.DeleteItem(pid);
					Toast.makeText(getApplicationContext(), "Item deleted", Toast.LENGTH_SHORT).show();
					text.setText("");
					
				} else {
					if (insert.insertItem(pid, name, price, desc)) {
						Toast.makeText(getApplicationContext(), "Item added",
								Toast.LENGTH_SHORT).show();
						
						text.setTextColor(Color.RED);
						text.setText("Remove");
					}
				}
				insert.close();

			}

		});

	}

	
	class LoadProducts extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Home.this);
			pDialog.setMessage("Loading...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... arg0) {
			List<NameValuePair> params = new ArrayList<NameValuePair>();

			JSONObject json = jp.makeHttpRequest(url_products, "GET", params);

			Log.d("Response", json.toString());

			try {
				int success = json.getInt(TAG_SUCCESS);

				if (success == 1) {
					products = json.getJSONArray(TAG_PRODUCTS);

					for (int i = 0; i < products.length(); ++i) {
						JSONObject c = products.getJSONObject(i);

						String pid = c.getString(TAG_PID);
						String name = c.getString(TAG_NAME);
						String price = c.getString(TAG_PRICE);
						String desc = c.getString(TAG_DESC);

						HashMap<String, String> map = new HashMap<String, String>();

						map.put(TAG_PID, pid);
						map.put(TAG_NAME, name);
						map.put(TAG_PRICE, price);
						map.put(TAG_DESC, desc);

						productList.add(map);

					}
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			pDialog.dismiss();

			runOnUiThread(new Runnable() {

				@Override
				public void run() {

					ListAdapter adapter = new SimpleAdapter(Home.this,
							productList, R.layout.item_list, new String[] {
									TAG_PID, TAG_NAME, TAG_PRICE, TAG_DESC },
							new int[] { R.id.pid, R.id.name, R.id.price,
									R.id.desc });

					setListAdapter(adapter);
				}
			});

		}

	}
}
