package com.steers.steer;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Login extends Activity {
	//Boolean isInternetPresent = false;
	//ConnectionDetector cd;

	/*SharedPreferences pref;
	private String prefName = "LoginDetails";

	private static final String EMAIL = "email";
	private static final String PASSWORD = "password";
*/
	EditText email, password;
	Button login;
	TextView registerHere, Error;
	private ProgressDialog pDialog;

	JSONP json = new JSONP();
	private static final String TAG_SUCCESS = "success";
	private static String login_url = "http://192.168.43.65:81/mduuka/api/login.php";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.login);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title);
		
		// Title menu
		TextView login = (TextView) findViewById(R.id.tvDetails);
		login.setText("Login");
		
		
		email = (EditText) findViewById(R.id.etLoginEmail);
		password = (EditText) findViewById(R.id.etLoginPassword);
		login = (Button) findViewById(R.id.btnLogin);
		registerHere = (TextView) findViewById(R.id.tvRegisterHere);
		
		registerHere.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(Login.this, Register.class));
				finish();
			}
		});
		login.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
					new signin().execute();
			}
		});
	}

	public void showAlertDialog(Context context, String title, String message,
			Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setIcon(R.drawable.no);
		alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {

			}
		});

		alertDialog.show();
	}

	class signin extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Login.this);
			pDialog.setMessage("Logging in...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... arg0) {
			Encription encryt = new Encription(password.getText().toString());
			
			String Email = email.getText().toString();
			String Password = encryt.md5();

			List<NameValuePair> param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair("email", Email));
			param.add(new BasicNameValuePair("password", Password));

			JSONObject j = json.makeHttpRequest(login_url, "POST", param);
			Log.d("Create response ", j.toString());

			try {
				int success = j.getInt(TAG_SUCCESS);
				if (success == 1) {
					
					startActivity(new Intent(Login.this, Home.class));
					finish();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			pDialog.dismiss();
		}

	}
}
